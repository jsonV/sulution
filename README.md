# SulutionPlugin
## Introduction
This is a BakkesMod plugin for Rocket League. It is a timing plugin that will time how long it takes from spawn to hit the world, or hit the ball or score. Can be enabled in freeplay and custom trainings. Very handy to train being as fast as possible (fast aerials, drive without boost, ...).

Compile the plugin as a 32 bit dll with the bakkesmodsdk.
If you don't want to compile it, there's a dll and set file available in the  SulutionPlugin/Release directory.
### Usage
Put the dll in bakkesmod plugins directory, put the SulutionPlugin.set file in plugins/settings directory.

Modify the plugins.cfg file in your Bakkesmod\cfg installation folder to auto-load the SulutionPlugin via `plugin load sulutionplugin`. In order to use the plugin, hit `F2` to open the BakkesMod GUI, and under the `Plugins` tab, select the SulutionP Plugin and hit either the `Enable Sulution` or `Disable Sulution` buttons to enable, or disable, the plugin, respectively.

You may also adjust the timing restart delay between respawns, and show fast aerial details, if you require more information.

