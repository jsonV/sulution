#include "SulutionPlugin.h"
#include <iomanip> // setprecision
#include <sstream> // stringstream
#include <iostream>
#include <cstdio>
#define _USE_MATH_DEFINES
#include "bakkesmod\wrappers\GameEvent\ServerWrapper.h"
#include "bakkesmod\wrappers\GameObject\BallWrapper.h"
#include "bakkesmod\wrappers\GameObject\CarWrapper.h"
#include "bakkesmod\wrappers\GameObject\GoalWrapper.h"
#include "bakkesmod\wrappers\ArrayWrapper.h"
#include "bakkesmod\wrappers\WrapperStructs.h"
using namespace std::placeholders;

BAKKESMOD_PLUGIN(SulutionPlugin, "Sulution Plugin", "0.1", PLUGINTYPE_FREEPLAY | PLUGINTYPE_CUSTOM_TRAINING)
FILE *stream;

void SulutionPlugin::onLoad() {
	enabled = make_shared<bool>(false);
	waitStartTimer = make_shared<float>(1.f);
	fastAerials = make_shared<bool>(false);

	carJustReset = 10;

	gameWrapper->HookEvent("Function TAGame.Mutator_Freeplay_TA.Init", std::bind(&SulutionPlugin::OnWorldLoad, this, std::placeholders::_1));
	gameWrapper->HookEvent("Function TAGame.GameEvent_Soccar_TA.Destroyed", std::bind(&SulutionPlugin::OnWorldDestroy, this, std::placeholders::_1));

	gameWrapper->HookEvent("Function TAGame.GameEvent_Tutorial_TA.OnInit", std::bind(&SulutionPlugin::OnWorldLoad, this, std::placeholders::_1));
	gameWrapper->HookEvent("Function TAGame.GameEvent_Tutorial_TA.Destroyed", std::bind(&SulutionPlugin::OnWorldDestroy, this, std::placeholders::_1));

	cvarManager->registerCvar("su_enabled", "0", "Enables/disable Sulution", true, true, 0.f, true, 1.f)
		.addOnValueChanged(std::bind(&SulutionPlugin::OnEnabledChanged, this, std::placeholders::_1, std::placeholders::_2));
	cvarManager->getCvar("su_enabled").bindTo(enabled);
	cvarManager->registerCvar("su_wait_start_timer", "0.25", "Wait duration to restart timer", true, true, 0.1f, true, 2.f).bindTo(waitStartTimer);
	cvarManager->registerCvar("su_fastaerials", "0", "Enables/disable fast aerials info", true, true, 0.f, true, 1.f).bindTo(fastAerials);

	//drawables
	canvasLogsMaxCount = (sizeof(canvasLogs) / sizeof(*canvasLogs));
	canvasLogsIndex=0;
	canvasLogsActiveTimerIndex = 0;
	for (size_t i = 0; i < canvasLogsMaxCount; i++)
	{
		canvasLogs[i] = "";
	}

	gameWrapper->RegisterDrawable([this](CanvasWrapper cw) {
		if (!canBeEnabled()) {
			return;
		}

		if (*enabled) {
			this->drawTimerPanel(cw);
		}
	});
}

void SulutionPlugin::onUnload() {

}

void SulutionPlugin::drawStringAt(CanvasWrapper cw, std::string text, int x, int y, Color col)
{
	Vector2 vec = { x , y };
	cw.SetPosition(vec);
	cw.SetColor(col.r, col.g, col.b, col.a);
	cw.DrawString(text);
}

void SulutionPlugin::drawStringAt(CanvasWrapper cw, std::string text, Vector2 loc, Color col)
{
	drawStringAt(cw, text, loc.X, loc.Y, col);
}

void SulutionPlugin::drawTimerPanel(CanvasWrapper cw) //, int x, int y)
{
	Vector2 size = cw.GetSize();

	int marginLeft = 10;
	int marginTop = 10;

	int width = 400;
	int height = 300;

	int lineSpacing = 20;

	int panelMargin = 10;
	int x = size.X - panelMargin - width;
	int y = panelMargin;

	Vector2 vec = { x , y };
	cw.SetPosition(vec);
	cw.SetColor(COLOR_PANEL);
	Vector2 vec2 = { width, height };
	cw.FillBox(vec2);
	cw.SetColor(COLOR_TEXT);

	int currentY = y + marginTop;

	if (timerReady) {
		drawStringAt(cw, "0.000 seconds", x + marginLeft, currentY, { COLOR_TIMER_RESET });
		currentY += lineSpacing;
	} else if (timerStarted) {
		float time = GetSecondsElapsed() - timerStartTime;

		stringstream stream;
		stream << fixed << setprecision(3) << time;
		string s = stream.str();

		drawStringAt(cw, s+" seconds", x + marginLeft, currentY, { COLOR_TIMER });

		currentY += lineSpacing;
	}
	else {
		currentY += lineSpacing;
	}

	currentY += lineSpacing;

	//browse and display canvasLogs

	int index = canvasLogsIndex;

	Color textColor = { COLOR_TEXT_UNACTIVE };
	bool activeReached = false;
	if (index == canvasLogsActiveTimerIndex) activeReached = true; 
	for (int i = 0; i < canvasLogsMaxCount; i++)
	{
		if (!activeReached) {
			if (index == canvasLogsActiveTimerIndex) {
				activeReached = true;
				textColor = { COLOR_TEXT };
			}
		}
		drawStringAt(cw, canvasLogs[index], x + marginLeft, currentY, textColor);
		currentY += lineSpacing;

		index++;
		if (index >= canvasLogsMaxCount) index -= canvasLogsMaxCount;
	}
}
void SulutionPlugin::canvasLog(std::string msg) {
	canvasLogs[canvasLogsIndex] = msg;
	canvasLogsIndex++;
	if (canvasLogsIndex >= canvasLogsMaxCount) canvasLogsIndex -= canvasLogsMaxCount;
}
CarWrapper SulutionPlugin::GetGameCar() {
	ServerWrapper training = gameWrapper->GetGameEventAsServer();
	if (training.IsNull()) {
		return false;
	}
	return training.GetGameCar();
}
float SulutionPlugin::GetSecondsElapsed() {
	ServerWrapper training = gameWrapper->GetGameEventAsServer();
	if (training.IsNull()) {
		return false;
	}
	return training.GetSecondsElapsed();
}
void SulutionPlugin::OnHitBall(std::string eventName) {
	if (!canBeEnabled()) return;
	if (!*enabled) return;
	//log("hit ball");
	if (timerStarted) {
		if (timerHitBall < 1) { //display it once only
			timerDisplay("hit ball");
			timerHitBall++;
		}
	}
}
void SulutionPlugin::OnHitWorld(std::string eventName) {
	if (!canBeEnabled()) return;
	if (!*enabled) return;
	//log("hit world");
	if (timerStarted) {
		if (timerHitWorld < 1) { //display it once only
			timerDisplay("hit world");
			timerHitWorld++;
		}
	}
}
void SulutionPlugin::OnBallHitGoal(std::string eventName) {
	if (!canBeEnabled()) return;
	if (!*enabled) return;
	//log("hit goal");
	if (timerStarted) {
		if (timerScore < 1) { //display it once only
			timerDisplay("score");
			timerScore++;
		}
	}
}
void SulutionPlugin::OnJumpPressed(std::string eventName) {
	if (!canBeEnabled()) return;
	if (!*enabled) return;
	if (!timerStarted) { //start timer if not already done by car moving detection
		timerIsReady();
		timerStart();
	}
	if (timerStarted && *fastAerials) {
		if (timerJumpPressed < 2) { //display it twice only
			jumpPressedTime = GetSecondsElapsed();
			if (timerJumpPressed == 0) {
				timerDisplay("1st jump");
			}
			else {
				float duration = jumpPressedTime - jumpReleaseTime;
				stringstream stream0;
				stream0 << fixed << setprecision(3) << duration;
				string s0 = stream0.str();
				stringstream stream1;
				stream1 << fixed << setprecision(2) << GetGameCar().GetLocation().Z;
				string s1 = stream1.str();
				timerDisplay("2nd jump +"+s0+"s, Z="+s1);
			}
			timerJumpPressed++;
		}
	}
}
void SulutionPlugin::OnJumpReleased(std::string eventName) {
	if (!canBeEnabled()) return;
	if (!*enabled) return;
	if (timerStarted && *fastAerials) {
		if (timerJumpReleased < 1) { //display it once only

			jumpReleaseTime = GetSecondsElapsed();
			float duration = jumpReleaseTime - jumpPressedTime;

			stringstream stream0;
			stream0 << fixed << setprecision(3) << duration;
			string s0 = stream0.str();


			stringstream stream1;
			stream1 << fixed << setprecision(2) << GetGameCar().GetLocation().Z;
			string s1 = stream1.str();

			timerDisplay("1st jump hold duration "+s0+"s, Z=" + s1);
			timerJumpReleased++;
		}
	}
}
void SulutionPlugin::OnCarSpawn(std::string eventName) {
	carJustReset = 10;
}
void SulutionPlugin::OnFireBall(std::string eventName)
{
	timerIsReady();
	timerStart();
	timerDisplay("fire ball");
}
void SulutionPlugin::OnTick(CarWrapper cw, void* params, string funcName)
{
	if (gameWrapper->IsInGame() && *enabled && !cw.IsNull())
	{
		ServerWrapper server = gameWrapper->GetGameEventAsServer();
		if (server.IsNull()) return; 
		if (cw.IsNull()) return; 
		BallWrapper ball = server.GetBall();
		if (ball.IsNull()) return; 
		Vector carLocation = cw.GetLocation();
		// Check if car was iddle for more than X seconds
		if (fabsf(carLocation.X - lastCarLocation.X) + fabsf(carLocation.Y - lastCarLocation.Y) + fabsf(carLocation.Z - lastCarLocation.Z) < 0.1f) {
			if (!timerReady) {
				if (carIdle) {
					//if idle for more than x second or idle after just being reset
					float waitDuration = *waitStartTimer;
					if (carJustReset > 0) waitDuration = 0.1f;
					if (GetSecondsElapsed() - carIdleStartTime > waitDuration) {
						timerIsReady();
						carJustReset = 0;
					}
				}
				else {
					carIdle = true;
					carIdleStartTime = GetSecondsElapsed();
				}
			}
		}
		else {
			carIdle = false;
		}

		if (timerStarted) {
			if (*fastAerials) {
				float timeElpased = GetSecondsElapsed() - timerStartTime;
				if (timerFastAerialBilan0 == 0) {
					if (timeElpased >= timerFastAerialBilan0Time) {
						stringstream stream1;
						stream1 << fixed << setprecision(2) << GetGameCar().GetLocation().Z;
						string s1 = stream1.str();
						timerDisplay("Z=" + s1, timerFastAerialBilan0Time);
						timerFastAerialBilan0++;
					}
				}
				if (timerFastAerialBilan1 == 0) {
					if (timeElpased >= timerFastAerialBilan1Time) {
						stringstream stream1;
						stream1 << fixed << setprecision(2) << GetGameCar().GetLocation().Z;
						string s1 = stream1.str();
						timerDisplay("Z=" + s1, timerFastAerialBilan1Time);
						timerFastAerialBilan1++;
					}
				}
			}
		}
		else {
			if (timerReady) {
				if (!carIdle) {
					timerStart(); 
				}
			} 
		}
		lastCarLocation = carLocation;
	}
}
void SulutionPlugin::log(std::string msg) {
	cvarManager->log(msg);
}
bool SulutionPlugin::canBeEnabled() {
	return gameWrapper->IsInFreeplay() || gameWrapper->IsInCustomTraining() || gameWrapper->IsInGame();
}
void SulutionPlugin::enable()
{
	timerStarted = false;
	lastCarLocation.X = lastCarLocation.Y = lastCarLocation.Z = 0;
	carIdle = false;
	timerReady = false;
	gameWrapper->HookEventWithCaller<CarWrapper>("Function TAGame.Car_TA.SetVehicleInput", std::bind(&SulutionPlugin::OnTick, this, _1, _2, _3));
	gameWrapper->HookEvent("Function TAGame.Car_TA.EventHitBall", std::bind(&SulutionPlugin::OnHitBall, this, std::placeholders::_1));
	gameWrapper->HookEvent("Function TAGame.Car_TA.EventHitWorld", std::bind(&SulutionPlugin::OnHitWorld, this, std::placeholders::_1));
	gameWrapper->HookEvent("Function TAGame.GameEvent_Soccar_TA.HandleHitGoal", std::bind(&SulutionPlugin::OnBallHitGoal, this, std::placeholders::_1));
	gameWrapper->HookEvent("Function TAGame.Car_TA.OnJumpPressed", std::bind(&SulutionPlugin::OnJumpPressed, this, std::placeholders::_1));
	gameWrapper->HookEvent("Function TAGame.Car_TA.OnJumpReleased", std::bind(&SulutionPlugin::OnJumpReleased, this, std::placeholders::_1));
	gameWrapper->HookEvent("Function TAGame.Car_TA.PostBeginPlay", std::bind(&SulutionPlugin::OnCarSpawn, this, std::placeholders::_1));
	gameWrapper->HookEvent("Function TAGame.GameEvent_GameEditor_TA.FireBalls", std::bind(&SulutionPlugin::OnFireBall, this, std::placeholders::_1));

}
void SulutionPlugin::disable()
{
	gameWrapper->UnhookEvent("Function TAGame.Car_TA.SetVehicleInput");
	gameWrapper->UnhookEvent("Function TAGame.Car_TA.EventHitBall");
	gameWrapper->UnhookEvent("Function TAGame.Car_TA.EventHitWorld");
	gameWrapper->UnhookEvent("Function TAGame.GameEvent_Soccar_TA.HandleHitGoal");
	gameWrapper->UnhookEvent("Function TAGame.Car_TA.OnJumpPressed");
	gameWrapper->UnhookEvent("Function TAGame.Car_TA.OnJumpReleased");
	gameWrapper->UnhookEvent("Function TAGame.Car_TA.PostBeginPlay");
	gameWrapper->UnhookEvent("Function TAGame.GameEvent_GameEditor_TA.FireBalls");
}
float SulutionPlugin::getCarTilt() {
	return 0;
}
void SulutionPlugin::timerIsReady()
{
	timerReady = true;
	timerStarted = false;
	cvarManager->log("Sulution timer ready");
}
void SulutionPlugin::timerStart()
{
	timerStarted = true;
	timerReady = false;
	carIdle = false;
	carIdleStartTime = GetSecondsElapsed();
	timerHitBall = timerHitWorld = timerScore = timerJumpPressed = timerJumpReleased = 0;
	timerFastAerialBilan0 = timerFastAerialBilan1 = 0;
	timerFastAerialBilan0Time = 0.6f;
	timerFastAerialBilan1Time = 1.2f;
	timerStartTime = GetSecondsElapsed();
	canvasLogsActiveTimerIndex = canvasLogsIndex;
	cvarManager->log("Sulution timer starts");
}

float SulutionPlugin::timerDisplay(std::string category, float timeElapsed)
{
	float time = GetSecondsElapsed() - timerStartTime;
	if (timeElapsed >= 0) time = timeElapsed;
	stringstream stream;
	stream << fixed << setprecision(3) << time;
	string s = stream.str();
	cvarManager->log("Sulution timer "+category+" : "+s+"s");
	canvasLog(s + "s (" + category + ")");
	return time;
}
void SulutionPlugin::OnWorldLoad(std::string eventName)
{
	cvarManager->log("OnWorldLoad");
	if (*enabled) enable();
}
void SulutionPlugin::OnWorldDestroy(std::string eventName)
{
	cvarManager->log("OnWorldDestroy");
	disable();
}
void SulutionPlugin::OnEnabledChanged(std::string oldValue, CVarWrapper cvar)
{
	auto cvarName = cvar.getCVarName();
	if (cvar.getBoolValue() && canBeEnabled()) enable();
	else disable();
}
